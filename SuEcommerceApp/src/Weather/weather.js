import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import useWeather from './useWeather';

const WeatherScreen = () => {
  const {data} = useWeather({
    lat: 16.871311,
    lon: 96.199379,
  });
  console.log('weather:', data);
  return (
    <View style={[styles.container]}>
      <Text style={{fontSize: 20, color: 'blue',fontWeight:'bold'}}>Weather</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default WeatherScreen;
