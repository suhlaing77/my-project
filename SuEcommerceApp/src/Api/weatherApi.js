import axios from "axios"

export const getWeatherApi = ({lat,lon}) => {
    return new Promise((resolve,reject) => {
        axios({
            method: 'GET',
            url: `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&units=metric&exclude=hourly,minutely&appid=6ddddc5de502e9fccd66ba3158e5df0f`,
            headers:{}
        })
        .then(response => {
            resolve(response?.data)
        })
        .catch(error => {
            console.log('api:',error)
            reject(error)
        })
    })
}