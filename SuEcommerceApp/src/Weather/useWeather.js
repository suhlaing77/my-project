import {useCallback, useEffect, useState} from 'react';
import {getWeatherApi} from '../Api/weatherApi';

export default useWeather = ({lat, lon}) => {
  const [data, setData] = useState({});

  const fetchWeather = useCallback(() => {
    getWeatherApi({lat, lon})
      .then(response => {
        setData(response);
      })
      .catch(error => {
        console.log('error:', error);
      });
  }, [lat, lon]);

  useEffect(() => {
    fetchWeather();
  }, [lat, lon]);

  return {data};
};
